---
title: Lois de Descartes
---

## Définitions

- Dioptre : surface de séparation entre deux milieux d'indices différents.
- Plan d'incidence : défini par la normale au dioptre et le rayon incident.
