---
title: Dérivabilité
layout: '$layouts/MarkdownLayout.astro'
---

## Dérivées des fonctions usuelles

| Fonction $f$       | Dérivée $f'$         | Ensembles de dérivabilité |
|--------------------|----------------------|---------------------------|
| $f(x) = k$         | $f'(x) = 0$          | $\mathbb{R}$              |
| $f(x) = x$         | $f'(x) = 1$          | $\mathbb{R}$              |
| $f(x) = ax + b$    | $f'(x) = a$          | $\mathbb{R}$              |
| $f(x) = x^2$       | $f'(x) = 2x$         | $\mathbb{R}$              |

## Formules

- $(e^{u(x)})' = u'(x) \times e^x$

