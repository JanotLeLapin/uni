---
title: Trigonométrie
layout: '$layouts/MarkdownLayout.astro'
---

## Formules

|       _      |      $0$      |      $\frac{\pi}{6}$      |      $\frac{\pi}{4}$      |      $\frac{\pi}{3}$      |       $\frac{\pi}{2}$       |
|--------------|---------------|---------------------------|---------------------------|---------------------------|-----------------------------|
| $sin x$      | $0$           | $\frac{1}{2}$             | $\frac{\sqrt{2}}{2}$      | $\frac{\sqrt{3}}{2}$      | $1$                         |
| $cos x$      | $1$           | $\frac{\sqrt{3}}{2}$      | $\frac{\sqrt{2}}{2}$      | $\frac{1}{2}$             | $0$                         |

### Dérivées

- $(sin x)' = cos x$
- $(cos x)' = - sin x$
- $(arctan x)' = \frac{1}{1 + x^2}$
