---
title: Développement limité
layout: '$layouts/MarkdownLayout.astro'
---

## Formules de Taylor-Young

- $e^x = 1 + x + \frac{x^2}{2!} + \frac{x^3}{3!} + \frac{x^4}{4!} + ... + \frac{x^n}{n!} + o(x^n)$
- $cos(x) = 1 - \frac{x^2}{2!} + \frac{x^4}{4!} - ... + (-1)^n \frac{x^{2n}}{(2n)!} + o(x^{2n+1})$
- $sin(x) = x - \frac{x^3}{3!} + \frac{x^5}{5!} + (-1)^n \frac{x^{2n+1}}{(2n+1)!} + o(x^{2n+2})$
- $(1+x)^{\alpha} = 1 + \alpha x + \frac{\alpha(\alpha - 1)}{2}x^2 + ... + \frac{\alpha(\alpha - 1) ... (\alpha - n + 1)}{n!} x^n + o(x^n)$
- $\frac{1}{1 - x} = 1 + x + x^2 + ... + x^n + o(x^n)$
- $ln(1 + x) = x - \frac{x^2}{2} + \frac{x^3}{3} - ... + (-1)^{n-1} \frac{x^n}{n} + o(x^n)$
- $ln(1 - x) = -x - \frac{x^2}{2} - \frac{x^3}{3} - ... - \frac{x^n}{n} + o(x^n)$
