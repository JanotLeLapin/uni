---
title: Formules
layout: '$layouts/MarkdownLayout.astro'
---

## Formules

Soit $(u_n)$ une suite arithmétique :

$\sum_{k=0}^{n}(u_n) = \frac{n+1}{2}(u_0 + u_n)$

Soit $(u_n)$ une suite géométrique :

$\sum_{k=0}^{n}(u_n) = \frac{1 - q^{n+1}}{1-q}$ si $q \neq 1$
