---
title: Méthodologie
---

Une assertion en mathématiques est une expression qui peut être vraie ou fausse.

## Connecteurs logiques

On peut construire des assertions mathématiques en utilisant des connecteurs logiques :

- **non($P$)** : faux si $P$ est vraie, et inversement.
- **$P$ et $Q$** : vrai seulement si $P$ et $Q$ sont vrais.
- **$P$ ou $Q$** : vrai si l'un d'entre $P$ et $Q$ est vrai.

## Tables de vérité

Soient $P$ et $Q$ des propositions :

| $P$ | $Q$ | $P$ et $Q$ | $P$ ou $Q$ | non $P$ | non(non($P$)) | non($Q$) | non($P$ et $Q$) | (non($P$)) ou (non($Q$)) |
|-----|-----|------------|------------|---------|---------------|----------|-----------------|--------------------------|
| V   | V   | V          | V          | F       | V             | F        | F               | F                        |
| V   | F   | F          | V          | F       | V             | V        | V               | V                        |
| F   | V   | F          | V          | V       | F             | F        | V               | V                        |
| F   | F   | F          | F          | V       | F             | V        | V               | V                        |

Soient $P$ et $Q$ des propositions :

| $P$ | $Q$ | $P \Rightarrow Q$ | $Q \Rightarrow P$ | $P \Leftrightarrow Q$ |
|-----|-----|-------------------|-------------------|-----------------------|
| V   | V   | V                 | V                 | V                     |
| V   | F   | F                 | V                 | F                     |
| F   | V   | V                 | F                 | F                     |
| F   | F   | V                 | V                 | V                     |
