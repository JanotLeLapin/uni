---
title: Formules
layout: '$layouts/MarkdownLayout.astro'
---

## Négations

- $non(x \in S \cup T) \Leftrightarrow x \notin S \cap T$
- $non(\forall x \in S) \Leftrightarrow \exists x \in S$
