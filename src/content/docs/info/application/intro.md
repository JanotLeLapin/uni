---
title: Applications de l'informatique
---

L'informatique est une discipline scientifique issue de trois autres disciplines :

- **Mathématiques discrètes** (+ probabilités et stats)
- **Sciences humaines et sociales** (+ linguistique)
- **Physique** (+ composants)

Les ordinateurs utilisent un système de numération **binaire**, c'est-à-dire un système de numération qui utilise seulement deux chiffres. Ce système fonctionne bien pour coder des valeurs entières, mais ne permet pas de coder certaines fractions.
