import { defineConfig } from 'astro/config';

import starlight from "@astrojs/starlight";

// https://astro.build/config
export default defineConfig({
  site: 'https://janotlelapin.codeberg.page',
  base: '/uni',
  integrations: [
    starlight({
      title: 'Mes Cours',
      defaultLocale: 'fr',
      locales: {
        root: {
          label: 'Français',
          lang: 'fr',
        },
      },
      social: {
        github: 'https://github.com/JanotLeLapin',
        codeberg: 'https://codeberg.org/JanotLeLapin',
      },
      customCss: ['./src/styles.css'],
      sidebar: [
        {
          label: '💻 Informatique',
          items: [
            { label: '🐍 Python', autogenerate: { directory: 'info/python' } },
            { label: '📐 Applications', autogenerate: { directory: 'info/application' } }
          ],
        },
        {
          label: '🧮 Mathématiques',
          items: [
            { label: 'Algèbre', autogenerate: { directory: 'maths/algebre' } },
            { label: 'Générale', autogenerate: { directory: 'maths/generale' } },
            { label: 'Méthodologie', autogenerate: { directory: 'maths/methodologie' } },
          ],
        },
        {
          label: '🔬 Physique',
          items: [
            { label: '👁️ Optique', autogenerate: { directory: 'physique/optique' } },
            { label: '🔩 Mécanique', autogenerate: { directory: 'physique/mecanique' } },
          ],
        },
      ]
    }),
  ],
  markdown: {
    shikiConfig: {
      theme: 'github-light'
    },
    remarkPlugins: ['remark-math'],
    rehypePlugins: [['rehype-katex', {}]]
  },
});
