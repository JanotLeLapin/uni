# Emploi du Temps

Pour installer les dépendances :

```bash
bun install
```

Pour exécuter :

```bash
bun run index.ts
```
