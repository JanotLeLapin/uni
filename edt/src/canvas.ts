import { type Event } from "./api"

import { createCanvas } from "canvas"

const WIDTH = 500;

const normalizeDate = (d: Date) => (d.getHours() + d.getMinutes() / 60) - 8

export const drawEdt = (edt: Event[]): Buffer => {
  const canvas = createCanvas(WIDTH, 1100);
  const ctx = canvas.getContext("2d");

  edt.forEach(e => {
    const start = normalizeDate(e.start);

    const y = start * 100;
    const h = (normalizeDate(e.end) - start) * 100;

    ctx.fillStyle = e.color;
    ctx.fillRect(8, y, WIDTH-16, h);

    ctx.font = "28px monospace";
    ctx.fillStyle = "#000000";
    ctx.fillText(e.description.subject, 16, y + h / 2, WIDTH-32);
  });

  for (let i = 1; i < 900; i++) {
    ctx.beginPath();
    ctx.strokeStyle = "white";
    ctx.moveTo(16, i * 100);
    ctx.lineTo(WIDTH-16, i * 100);
    ctx.stroke();
  }

  return canvas.toBuffer('image/png')
}
