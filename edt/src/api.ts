import { decode } from "he"
import { nextDay } from "./util"

export type Description = {
  type: string
  location: string
  subject: string
}

export type Event = {
  id: string
  start: Date
  end: Date
  description: Description
  category: string
  sites: string
  color: string
}

const PATTERN = "\r\n\r\n<br />\r\n\r\n";
const parseDescription = (description: string): Description => {
  const res = description.split(PATTERN);
  const [type, location, subject] = res.map(el => decode(el));
  return {
    type,
    location,
    subject: subject.split("-")[1].split(" [")[0],
  }
}

const formatDate = (date: Date) => `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;

export const fetchEdt = async (date: Date): Promise<Event[]> => {
  const body = `start=${formatDate(date)}&end=${formatDate(nextDay(date))}&resType=103&calView=agendaDay&federationIds%5B%5D=S1MITD-01&colourScheme=3`;
  console.log(body);

  const res: any[] = await fetch("https://edt.uvsq.fr/Home/getCalendarData", {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    body,
  }).then(res => res.json());

  return res
    .map(obj => ({
      id: obj.id,
      start: new Date(Date.parse(obj.start)),
      end: new Date(Date.parse(obj.end)),
      category: obj.eventCategory,
      description: parseDescription(obj.description),
      sites: obj.sites.join(", "),
      color: obj.backgroundColor,
    }) as unknown as Event)
    .filter(obj => obj.start.getDate() === date.getDate())
}
