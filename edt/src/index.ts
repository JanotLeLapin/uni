import { fetchEdt, type Event } from "./api"
import { drawEdt } from "./canvas"

import { AttachmentBuilder, type BaseGuildTextChannel, Client } from "discord.js"

import { config } from "dotenv"
import { nextDay } from "./util"
config()

const client = new Client({
  intents: ["MessageContent", "Guilds", "GuildMessages", "GuildMessageTyping", "GuildMessageReactions"],
})

client.on("messageCreate", async (msg) => {
  if (!msg.content.startsWith("/")) return;
  const [cmd, ...args] = msg.content.substring(1).split(" ");

  if (cmd !== "edt") return;
  let date = new Date(args.length ? Date.parse(args[0]) : Date.now());
  date = nextDay(date);

  let edt: Event[];
  try {
    edt = await fetchEdt(date);
  } catch (err) {
    msg.channel.send("Impossible de charger l'emploi du temps.");
    return;
  }
  edt.sort((a, b) => a.start.getTime() - b.start.getTime());

  const attachment = new AttachmentBuilder(drawEdt(edt), { name: 'edt.png' });

  const channel = await client.channels.fetch("1151072301127835674") as BaseGuildTextChannel;
  channel.send({
    content: `@everyone Emploi du temps du **${date.getDate()}**/**${date.getMonth()}**/**${date.getFullYear()}**`,
    files: [ attachment ],
  })
})

client.login(process.env.TOKEN);
